<?php // $Id$  ?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">


<?php print $picture ?>

<?php if ($teaser) { ?>

<!-- Teaser here -->

 <div class="meta">

     <?php if ($submitted): ?>
    <span class="submitted">Posted by <?php print $user_name ?> on <?php print $date ?></span>
  <?php endif; ?> 


  <?php if ($terms): ?>
    <div class="terms terms-inline">Tags: <?php print $terms ?></div>
  <?php endif;?>
  
   <?php if ($links): ?>
   <div class="lower-node-links">
  <?php print $links; ?>
  </div>
   <?php endif;?>
    	&nbsp;
  </div>
  

<div class="node-content">

<?php if (!$page): ?>
  <span class="node-title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></span>
<?php endif; ?>


  <div class="content">
    <?php print $content ?>
    
    

   
 
   
 
 
     
     </div>
     
      <?php     if($node->readmore == TRUE) {
   // adds read more link as image to the bottom of the teaser content
   ?>
    
  <?php  print "<a href='/" . $node->links["node_read_more"]["href"] . "' class='readmore' title='" . $node->links["node_read_more"]["attributes"]["title"] . "..' >" .  '<img src="' . base_path() . path_to_theme() . '/images/read-more.gif" width="120" height="40" alt="" />' . "</a>"; ?>
  
<?php

}

?>
 

</div>
 <?php } else { ?>

<!-- Full node here -->


<?php if (!$page): ?>
  <span class="node-title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></span>
<?php endif; ?>

  <div class="meta">
  <?php if ($submitted): ?>
    <span class="submitted">Posted by <?php print $user_name ?> on <?php print $date ?></span>
  <?php endif; ?>


  <?php if ($terms): ?>
    <div class="terms terms-inline">Tags: <?php print $terms ?></div>
  <?php endif;?>
  </div>

  <div class="content">
    <?php print $content ?>
  </div>
<div class="node-bottom-links">
  <?php print $links; ?>
  </div>



<?php } ?>




</div>
   