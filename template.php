<?php // $Id$  ?>
<?php


/* These are needed for the body classes Do not remove them */

function scruffy_desk_preprocess_page(&$vars, $hook) {

    $classes = split(' ', $vars['body_classes']);
    
     if(empty($vars['right'])){
        $classes[] = t('no-right-column');
    } else{
        $classes[] = t('has-right-column');
    }
    if(empty($vars['footer_four'])){
        $classes[] = t('no-footer-four');
    } else{
        $classes[] = t('has-footer-four');
    }
  
    
    $vars['body_classes_array'] = $classes;
    $vars['body_classes'] = implode(' ', $classes);
    
}




/* Breadcrumb

This replaces the breadcrumb seperator with an arrow

*/


function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
// uncomment the next line to enable current page in the breadcrumb trail
 $breadcrumb[] = drupal_get_title();
    return '<div class="breadcrumb">'. implode(' &rarr; ', $breadcrumb) .'</div>';
  }
}




// Removes read more link from its usual node position

function scruffy_desk_links($links, $attributes = array()) {
    unset($links['node_read_more']);
    return theme_links($links, $attributes);
}



/* Node date style and username  - Used in node.tpl*/

function scruffy_desk_preprocess_node(&$vars) {

$node = $vars['node'];
$vars['date'] = format_date($node->created, 'custom', "l, F j, Y");

$vars['user_name'] = $vars['name'];
}

/**

Below changes the layout of the search box


* Override or insert PHPTemplate variables into the search_theme_form template.
*
* @param $vars
*   A sequential array of variables to pass to the theme template.
* @param $hook
*   The name of the theme function being called (not used in this case.)
*/


function scruffy_desk_preprocess_search_theme_form(&$vars, $hook) {

  // Modify elements of the search form
  $vars['form']['search_theme_form']['#title'] = t('');
 
  // Set a default value for the search box
 $vars['form']['search_theme_form']['#value'] = t('Search this Site');
 
  // Add a custom class to the search box
  $vars['form']['search_theme_form']['#attributes'] = array('class' => 'NormalTextBox txtSearch',
  'onfocus' => "if (this.value == 'Search this Site') {this.value = '';}",
  'onblur' => "if (this.value == '') {this.value = 'Search this Site';}");
 
  // Change the text on the submit button
  $vars['form']['submit']['#value'] = t('Search');

  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_theme_form']['#printed']);
  $vars['search']['search_theme_form'] = drupal_render($vars['form']['search_theme_form']);

  $vars['form']['submit']['#type'] = 'image_button';
  $vars['form']['submit']['#src'] =  'sites/all/themes/scruffy-desk/images/search.png';

  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}

/* Changes to the block search form */

function scruffy_desk_preprocess_search_block_form (&$vars, $hook) {

  // Modify elements of the search form
  $vars['form']['search_block_form']['#title'] = t('');
 
  // Set a default value for the search box
   //$vars['form']['search_block_form']['#value'] = t('Search this Site');
 
  // Add a custom class to the search box
  //$vars['form']['search_block_form']['#attributes'] = array('class' => 'NormalTextBox txtSearch',
  //'onfocus' => "if (this.value == 'Search this Site') {this.value = '';}",
  //'onblur' => "if (this.value == '') {this.value = 'Search this Site';}");
 
  // Change the text on the submit button
  $vars['form']['submit']['#value'] = t('Search');

  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_block_form']['#printed']);
  $vars['search']['search_block_form'] = drupal_render($vars['form']['search_block_form']);

  $vars['form']['submit']['#type'] = 'image_button';
  $vars['form']['submit']['#src'] =  'sites/all/themes/scruffy-desk/images/search.png';

  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}


/* Removes the automatic insertion of a double colon at the end of a form field */

function scruffy_desk_form_element($element, $value) {
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  $output = '<div class="form-item"';
  if (!empty($element['#id'])) {
    $output .= ' id="'. $element['#id'] .'-wrapper"';
  }
  $output .= ">\n";
  $required = !empty($element['#required']) ? '<span class="form-required" title="'. $t('This field is required.') .'">*</span>' : '';

  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="'. $element['#id'] .'">'. $t('!title !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
    else {
      $output .= ' <label>'. $t('!title !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
  }

  $output .= " $value\n";

  if (!empty($element['#description'])) {
    $output .= ' <div class="description">'. $element['#description'] ."</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}


/**
* Implementation of hook_theme.
*
* Register custom theme functions.
*/
/**
* This snippet will register a theme implementation of the user login form and the search form tpl.
**/


function scruffy_desk_theme(){
  return array(
    'user_login_block' => array(
      'template' => 'user-login-block',
      'arguments' => array('form' => NULL),
    ),
    
     'search_form' => array(
      'template' => 'search-form',
      'arguments' => array('form' => NULL),
    ),
    
  );
 
}



/* For the search form */
  
function scruffy_desk_preprocess_search_form(&$variables) {
  
    // Modify elements of the search form
  $variables['form']['basic']['#title'] = t('Search this site');
  
    $variables['form']['basic']['inline']['submit']['#type'] = 'image_button';
  $variables['form']['basic']['inline']['submit']['#src'] =  'sites/all/themes/scruffy-desk/images/search.gif';
 
  $variables['search_form'] = drupal_render($variables['form']);
  
}

/* For the user login form */

function scruffy_desk_preprocess_user_login_block(&$variables) {
  
    $variables['form']['name']['#title'] = '<img src="' . base_path() . path_to_theme() . '/images/username.png" width="150" height="20" alt="Username"/>';
  $variables['form']['pass']['#title'] = '<img src="' . base_path() . path_to_theme() . '/images/password.png" width="150" height="20" alt="Password"/>';
  
  $variables['form']['submit']['#type'] = 'image_button';
  $variables['form']['submit']['#src'] =  'sites/all/themes/scruffy-desk/images/login.png';
  
  $variables['rendered'] = drupal_render($variables['form']);
  
}