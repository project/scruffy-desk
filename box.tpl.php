<?php // $Id$  ?>
<div class="box">

<?php if ($title): ?>
  <p class="box-title"><?php print $title ?></p>
<?php endif; ?>

  <div class="content"><?php print $content ?></div>
</div>