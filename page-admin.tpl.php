<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>


<?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
  <!--[if IE 7]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie7.css";</style>
<![endif]-->
<!--[if IE 6]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie6.css";</style>
<![endif]-->



</head>

<body class="<?php print $body_classes; ?>">

<div id="admin-wrapper">
   
   <div id="header" class="clearfix">

   <?php if (!empty($breadcrumb)): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
   
     <?php if (!empty($secondary_links)): ?>
          <div class="navigation" >
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
          </div> <!-- /secondary-links -->
          <?php endif; ?>



<div id="logo-name">

    <?php if (!empty($logo)): ?>
	<div id="logo">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
		  </div> <!-- /logo -->
        <?php endif; ?>


 <?php if (!empty($site_name)): ?>
 <div id="site-name">
            <h1>
              <span><?php print $site_name; ?></span>
            </h1>
			</div> <!-- /site-name-->
          <?php endif; ?>

</div>

<?php if (!empty($primary_links)): ?>
          <div id="primary" class="clear-block">
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
          </div> <!-- /secondary-links -->
        <?php endif; ?>

</div>

<div id="mission-search-tag">

   <?php if (!empty($site_slogan)): ?>
            <div id="tagline"><?php print $site_slogan; ?>
                </div> <!-- /site-slogan -->
          <?php endif; ?>
    


  <?php if (!empty($search_box)): ?>
        <div id="search-box"><?php print $search_box; ?></div> <!-- /search-box -->
      <?php endif; ?>





</div>


<div id="container">

<div id="left">
 <?php print $left; ?>
</div> <!-- /left -->


<div id="content">
  <?php if (!empty($mission)): ?><div id="mission"><?php print $mission; ?></div> <!-- /mission -->
  <?php endif; ?>
  <?php if (!empty($title)): ?><h2 class="title" id="page-title"><?php print $title; ?></h2><?php endif; ?>
          <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
          <?php if (!empty($help)): print $help; endif; ?>
          <div id="content-content" class="clear-block">
            <?php print $content; ?>
          </div> <!-- /content-content -->
</div>

  <?php if (!empty($right)): ?>
        <div id="right" class="column sidebar">
          <?php print $right; ?>
        </div> <!-- /sidebar-right -->
      <?php endif; ?>




</div> <!-- /content -->


 <?php if (!empty($secondary_links)): ?>
          <div id="navigation" >
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
          </div> <!-- /secondary-links -->
          <?php endif; ?>



<div id="footer" class="clearfix">

  <?php if (!empty($footer_one)): ?>
        <div id="footer-one" class="column sidebar">
          <?php print $footer_one; ?>
        </div> <!-- /footer-one -->
      <?php endif; ?>

	  
	    <?php if (!empty($footer_two)): ?>
        <div id="footer-two" class="column sidebar">
          <?php print $footer_two; ?>
        </div> <!-- /footer-two -->
      <?php endif; ?>
	  
	  
 <?php if (!empty($footer_three)): ?>
        <div id="footer-three" class="column sidebar">
          <?php print $footer_three; ?>
        </div> <!-- /footer-three -->
      <?php endif; ?>
	  
	  
 <?php if (!empty($footer_four)): ?>
        <div id="footer-four" class="column sidebar">
          <?php print $footer_four; ?>
        </div> <!-- /footer-four -->
      <?php endif; ?>



	 
</div>


  <?php if (!empty($footer_message)): ?>
        <div id="footer-message">
           <?php print $footer_message; ?>
        </div> <!-- /footer-message -->
      <?php endif; ?>



  </div> <!-- /wrapper -->

  <?php print $closure; ?>
</body>
</html>